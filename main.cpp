#include <iostream>

using namespace std;

int main()
{
    cout << "Use Ctrl+C to exit.\n"; // info For exiting
    cout << "a Calculator written in 33 lines in C++\n";
    double NOne, NTwo,Full = 0;
    string Operator;
    cout << "First Number: ";
    cin >> NOne;
    cout << "Operator: ";
    cin >> Operator;
    cout << "Second Number: ";
    cin >> NTwo;
    if (Operator == "+") {
        Full = NOne + NTwo;
    } else if (Operator == "-") {
        Full = NOne - NTwo;
    } else if (Operator == "*") {
        Full = NOne * NTwo;
    } else if (Operator == "/") {
        Full = NOne / NTwo;
    }

    cout << Full;
    cout << "\n"; // This is there for Spacing in Terminal

    return 0;
}
